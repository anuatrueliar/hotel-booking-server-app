var bodyParser = require('body-parser');
const express = require('express')
const app = express()
const port = process.env.PORT || 3000;

var testController = '/app/controller/'

app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello World!'))


app.post('/printThisData', (req, res, next) => {
    console.log(req.body.firstName);
    console.log(req.body.lastName);
    res.send({ error: false});
});

app.listen(port, () => console.log(`Server started at port ${port}!`))